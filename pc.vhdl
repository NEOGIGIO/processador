library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc is
    port(
      pc_clk : in std_logic;
      pc_wr_en : in std_logic;
      pc_jump_en: in std_logic;
      pc_jump_control: in unsigned(6 downto 0);
      pc_data_out : out unsigned(6 downto 0)
    );
end entity;

architecture a_pc of pc is

  component reg7bits is
  port (
    clk : in std_logic;
    rst : in std_logic;
    wr_en : in std_logic;
    data_in : in unsigned(6 downto 0);
    data_out : out unsigned(6 downto 0)
  );
  end component;

  signal saida_reg : unsigned(6 downto 0);
  signal saida_pl : unsigned(6 downto 0) := "0000000";
begin
  reg0: reg7bits port map (
    clk => pc_clk, rst => '0', wr_en=>pc_wr_en,
    data_in => saida_pl, data_out => saida_reg
  );

  saida_pl <= saida_reg + 1 when pc_jump_en = '0' else
              pc_jump_control;
  pc_data_out <= saida_reg;

    
end architecture;