# Implementação de protótipo do Renesas v850

## CODIFICAÇÃO DAS INSTRUÇÕES:

* ADD reg1, reg2   (reg2 <= reg1 + reg2)
	formato:    0001    r1 r1 r1    r2 r2 r2    0000

* ADDI imm, reg1   (reg1 <= imm)
	formato:    0010    r1 r1 r1    i i i i i i i

* SUB reg1, reg2   (reg2 <= reg2 - reg1)
	formato:    0011    r1 r1 r1    r2 r2 r2    0000

* MOV reg1, reg2   (reg2 <= reg1)
	formato:    0100    r1 r1 r1    r2 r2 r2    0000

* JMP imm   
	formato:    0101    i i i i i i i    0000
	(adaptação: jump utilizando imediato em vez de valor obtido no reg1, como no Renesas)

* CMP reg1, reg2 (se reg2 - reg1 < 0 então flag s <= 1, senão flag s <= 0)
	formato:    0110    r1 r1 r1    r2 r2 r2    0000

* BLT imm   (se flag s = 1 então PC <= PC + imm)
	formato:    0111   i i i i i i i   000

* LD.W reg1, reg2   (reg2 <= load-memory(address <= reg1))
	formato:    1000    r1 r1 r1    r2 r2 r2    0000

* ST.W reg2, reg1   (store-memory(value <= reg2, address <= reg1))
	formato:    1001    r1 r1 r1    r2 r2 r2    0000