library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity state_machine is
    port(
        clk : in std_logic;
        toggle_en : in std_logic;
        reset : in std_logic;
        saida_q : out std_logic;
        saida_q_linha : out std_logic
    );
end entity;

architecture a_state_machine of state_machine is
signal estado : std_logic := '0';
    begin 
        saida_q <= estado when reset = '0' else '0';
        saida_q_linha <= not estado when reset = '0' else '1';
        process(clk)
        begin
            if(rising_edge(clk) and toggle_en = '1') then
                estado <= not estado;
            end if;
        end process;
end architecture;