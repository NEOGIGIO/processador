library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity control_unit is
    port(
        clk_pl : in std_logic;
        opcode : in unsigned(3 downto 0);
        pc_en : out std_logic;
        s_in : in std_logic; -- flag s do processador indicando negativo
        jump_en : out std_logic;
        data2_mux : out unsigned(1 downto 0); -- mux que ta na saida do data2 e entrada da ula
        wr_en_register : out std_logic;
        ula_op : out unsigned(1 downto 0);
        reg_sel_mux : out std_logic;
        cmp : out std_logic;
        ram_wr_en_control : out std_logic; -- when trying to store in a register. aka when doing STW
        ram_reg_st_ctrl : out std_logic -- when trying to store in a register. aka when doing LDW
    );
end entity;

architecture a_control_unit of control_unit is
component state_machine is
   port(
       clk : in std_logic;
       toggle_en : in std_logic;
       reset : in std_logic;
       saida_q : out std_logic;
       saida_q_linha : out std_logic
   );
end component;

signal estado : std_logic;

begin
    state_machine0: state_machine port map(clk=>clk_pl, toggle_en=>'1', reset=>'0', saida_q=>estado);
    pc_en <= '1' when estado = '0' or ((opcode = "0101" or (opcode = "0111" and s_in = '1')) and estado = '1') else '0';
    jump_en <= '1' when estado = '1' and (opcode = "0101" or (opcode = "0111" and s_in = '1')) else '0';
    data2_mux <= "00" when estado = '1' and (opcode = "0001" or opcode = "0011" or opcode="0110") else 
                "01" when estado = '1' and (opcode = "0010" or opcode = "0110") else 
                "10" when estado = '1' and (opcode = "0100")
                else "00";

    wr_en_register <= '1' when estado = '1' and (opcode = "0001" or opcode = "0011" or opcode = "0010" or opcode = "0100" or opcode = "1000") else '0';
    ula_op <= "01" when estado = '1' and (opcode = "0011" or opcode = "0110") else "00";
    reg_sel_mux <= '0' when estado = '1' and opcode = "0010" else '1';

    ram_reg_st_ctrl <= '1' when estado = '1' and opcode = "1000" else '0'; --LDW
    ram_wr_en_control <= '1' when estado = '1' and opcode = "1001" else '0'; --STW
    cmp <= '1' when estado = '1' and opcode = "0110" else '0';

end architecture;