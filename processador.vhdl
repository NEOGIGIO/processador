library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity processador is 
    port (
        clk_p : in std_logic;
        rst_p : in std_logic
    );
end entity;

architecture a_processador of processador is 
    component rom_pc_ula_register is
        port(
            clk_t : in std_logic;
            sel_ula_op: in unsigned(1 downto 0); -- operacao da ula
            rst_t : in std_logic; -- reset de todos os registradores do banco
            wr_en_t : in std_logic; -- write enable do banco de registradores
            pc_en_t : in std_logic;
            sel_reg_wr_t : in std_logic;
            jump_en_t : in std_logic;
            ram_in : in unsigned(7 downto 0);
            ram_reg_st_ctrl : in std_logic;
            mux2_control : in unsigned(1 downto 0);
            rom_out : out unsigned(13 downto 0);
            ula_out_t : out unsigned(7 downto 0);
            pc_out_t : out unsigned(6 downto 0);
            data1out_top : out unsigned(7 downto 0);
            data2out_top : out unsigned(7 downto 0)
        );
    end component;

    component reg1bit is
        port( clk : in std_logic;
            rst : in std_logic;
            wr_en : in std_logic;
            data_in : in std_logic;
            data_out : out std_logic
        );
    end component;
    
    component control_unit is
        port(
            clk_pl : in std_logic;
            opcode : in unsigned(3 downto 0);
            pc_en : out std_logic;
            s_in : in std_logic; -- flag s do processador indicando negativo
            jump_en : out std_logic;
            data2_mux : out unsigned(1 downto 0); -- mux que ta na saida do data2 e entrada da ula
            wr_en_register : out std_logic;
            ula_op : out unsigned(1 downto 0);
            reg_sel_mux : out std_logic;
            cmp : out std_logic;
            ram_wr_en_control : out std_logic;
            ram_reg_st_ctrl : out std_logic
        );
    end component;

    component ram is
        port(
            clk : in std_logic;
            endereco : in unsigned(6 downto 0); 
            wr_en : in std_logic;
            dado_in : in unsigned(7 downto 0); 
            dado_out : out unsigned(7 downto 0)
        );
    end component;

    signal instruction : unsigned (13 downto 0);
    signal ula_out, ram_out, reg1value, reg2value : unsigned (7 downto 0);
    signal pc_en, jump_en, wr_en_register, s_out, cmp_sig, ram_wr_en, reg_sel_mux, ram_reg_st_ctrl_sig : std_logic;
    signal data2_mux, ula_op  : unsigned(1 downto 0);

    begin
    control_unit0 : control_unit port map (
        clk_pl=>clk_p, 
        opcode=>instruction(13 downto 10), 
        pc_en=>pc_en,
        s_in=>s_out, 
        jump_en=>jump_en, 
        data2_mux=>data2_mux, 
        wr_en_register=>wr_en_register, 
        ula_op=>ula_op, 
        reg_sel_mux=>reg_sel_mux,
        cmp=>cmp_sig,
        ram_wr_en_control=>ram_wr_en,
        ram_reg_st_ctrl=>ram_reg_st_ctrl_sig
    );
    rom_pc_ula_register0 : rom_pc_ula_register port map (
        clk_t=>clk_p,
        sel_ula_op=>ula_op, 
        rst_t=>rst_p, 
        wr_en_t=>wr_en_register, 
        pc_en_t=>pc_en, 
        sel_reg_wr_t=>reg_sel_mux, 
        jump_en_t=>jump_en, 
        ram_in=>ram_out,
        ram_reg_st_ctrl=>ram_reg_st_ctrl_sig,
        mux2_control=>data2_mux,
        rom_out=>instruction,
        ula_out_t=>ula_out,
        data1out_top=>reg1value,
        data2out_top=>reg2value
    );

    ram0 : ram port map (
        clk=>clk_p,
        endereco=>reg1value(6 downto 0),
        wr_en=>ram_wr_en,
        dado_in=>reg2value,
        dado_out=>ram_out
    );

    s : reg1bit port map( clk=>clk_p,
    rst=>rst_p,
    wr_en=>cmp_sig,
    data_in=>ula_out(7),
    data_out=>s_out
    );



end architecture;