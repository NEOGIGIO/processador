library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_bank is
  port( clk_r : in std_logic;
  rst_r : in std_logic;
  wr_en_r : in std_logic;
  data1_reg : in unsigned(2 downto 0); -- select which reg is read in data1
  data2_reg : in unsigned(2 downto 0);
  data_reg_wr: in unsigned(2 downto 0); -- which reg data is written
  data_wr : in unsigned(7 downto 0); -- data to be written
  data1_out : out unsigned(7 downto 0);
  data2_out : out unsigned(7 downto 0)
  );
end entity;

architecture a_register_bank of register_bank is

  component reg8bits is
    port( clk : in std_logic;
    rst : in std_logic;
    wr_en : in std_logic;
    data_in : in unsigned(7 downto 0);
    data_out : out unsigned(7 downto 0)
    );
  end component;

  signal wr_en_sig2, wr_en_sig3, wr_en_sig4, wr_en_sig5, wr_en_sig6, wr_en_sig7, wr_en_sig8: std_logic;
  signal data_in_sig : unsigned(7 downto 0);
  signal data_out1_sig, data_out2_sig, data_out3_sig, data_out4_sig, data_out5_sig, data_out6_sig, data_out7_sig, data_out8_sig : unsigned(7 downto 0);

  begin
    --registers mapping
    reg1 : reg8bits port map(
      clk=>clk_r, rst=>'0',
      wr_en=>'1', data_in=>"00000000", data_out=>data_out1_sig);
    reg2 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig2, data_in=>data_in_sig, data_out=>data_out2_sig);
    reg3 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig3, data_in=>data_in_sig, data_out=>data_out3_sig);
    reg4 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig4, data_in=>data_in_sig, data_out=>data_out4_sig);
    reg5 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig5, data_in=>data_in_sig, data_out=>data_out5_sig);
    reg6 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig6, data_in=>data_in_sig, data_out=>data_out6_sig);
    reg7 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig7, data_in=>data_in_sig, data_out=>data_out7_sig);
    reg8 : reg8bits port map(
      clk=>clk_r, rst=>rst_r,
      wr_en=>wr_en_sig8, data_in=>data_in_sig, data_out=>data_out8_sig);


    data_in_sig <= data_wr;

    wr_en_sig2 <= '1' when data_reg_wr = "001" and wr_en_r = '1' else '0';
    wr_en_sig3 <= '1' when data_reg_wr = "010" and wr_en_r = '1' else '0';
    wr_en_sig4 <= '1' when data_reg_wr = "011" and wr_en_r = '1' else '0';
    wr_en_sig5 <= '1' when data_reg_wr = "100" and wr_en_r = '1' else '0';
    wr_en_sig6 <= '1' when data_reg_wr = "101" and wr_en_r = '1' else '0';
    wr_en_sig7 <= '1' when data_reg_wr = "110" and wr_en_r = '1' else '0';
    wr_en_sig8 <= '1' when data_reg_wr = "111" and wr_en_r = '1' else '0';

    data1_out <= data_out1_sig when data1_reg = "000" else
                 data_out2_sig when data1_reg = "001" else
                 data_out3_sig when data1_reg = "010" else
                 data_out4_sig when data1_reg = "011" else
                 data_out5_sig when data1_reg = "100" else
                 data_out6_sig when data1_reg = "101" else
                 data_out7_sig when data1_reg = "110" else
                 data_out8_sig when data1_reg = "111" else
                 "00000000";
    data2_out <= data_out1_sig when data2_reg = "000" else
                 data_out2_sig when data2_reg = "001" else
                 data_out3_sig when data2_reg = "010" else
                 data_out4_sig when data2_reg = "011" else
                 data_out5_sig when data2_reg = "100" else
                 data_out6_sig when data2_reg = "101" else
                 data_out7_sig when data2_reg = "110" else
                 data_out8_sig when data2_reg = "111" else
                 "00000000";
      
end architecture;




