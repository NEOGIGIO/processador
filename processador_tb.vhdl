library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity processador_tb is end entity;

architecture a_processador_tb of processador_tb is
    component processador is
        port (
            clk_p : in std_logic;
            rst_p : in std_logic
        );
    end component;

    signal clk, rst : std_logic;

    begin
        uut: processador port map(
            clk_p=>clk, 
            rst_p=>rst
        );

        process -- sinal de clock
        begin
            clk <= '0';
            wait for 50 ns;
            clk <= '1';
            wait for 50 ns;
        end process;

        process
        begin
            rst <= '1';
            wait for 50 ns;
            rst <= '0';
            wait;
        end process;

end architecture;