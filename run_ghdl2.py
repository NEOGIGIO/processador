import os
import sys

# project_name = sys.argv[1]

os.system("rm -rf work-obj93.cf")

# os.system("./ghdl -a {}.vhdl".format(project_name))
# os.system("./ghdl -e {}".format(project_name))

# os.system("./ghdl -a {}_tb.vhdl".format(project_name))
# os.system("./ghdl -e {}_tb".format(project_name))

# os.system("./ghdl -r {}_tb --stop-time=3000ns --wave={}_tb.ghw".format(project_name, project_name))

os.system("ghdl -a reg7bits.vhdl")
os.system("ghdl -e reg7bits")

os.system("ghdl -a reg7bits_tb.vhdl")
os.system("ghdl -e reg7bits_tb")

# os.system("ghdl -r reg7bits_tb --stop-time=1000ns --wave=reg7bits_tb.ghw")


os.system("ghdl -a reg8bits.vhdl")
os.system("ghdl -e reg8bits")

os.system("ghdl -a reg8bits_tb.vhdl")
os.system("ghdl -e reg8bits_tb")

# os.system("ghdl -r reg8bits_tb --stop-time=1000ns --wave=reg8bits_tb.ghw")


os.system("ghdl -a register_bank.vhdl")
os.system("ghdl -e register_bank")

os.system("ghdl -a register_bank_tb.vhdl")
os.system("ghdl -e register_bank_tb")

# os.system("ghdl -r register_bank_tb --stop-time=1600ns --wave=register_bank_tb.ghw")

os.system("ghdl -a ula.vhdl")
os.system("ghdl -e ula")

os.system("ghdl -a ula_tb.vhdl")
os.system("ghdl -e ula_tb")

os.system("ghdl -a ula_register.vhdl")
os.system("ghdl -e ula_register")

os.system("ghdl -a ula_register_tb.vhdl")
os.system("ghdl -e ula_register_tb")

# os.system("ghdl -r ula_tb --stop-time=300ns --wave=ula_tb.ghw")


# os.system("ghdl -a ula_plus_registers.vhdl")
# os.system("ghdl -e ula_plus_registers")

# os.system("ghdl -a ula_plus_registers_tb.vhdl")
# os.system("ghdl -e ula_plus_registers_tb")

os.system("ghdl -a state_machine.vhdl")
os.system("ghdl -e state_machine")

# os.system("ghdl -a state_machine_tb.vhdl")
# os.system("ghdl -e state_machine_tb")

# os.system("ghdl -a unidade_controle.vhdl")
# os.system("ghdl -e unidade_controle")

os.system("ghdl -a pc.vhdl")
os.system("ghdl -e pc")

os.system("ghdl -a pc_tb.vhdl")
os.system("ghdl -e pc_tb")

# os.system("ghdl -r pc_tb --stop-time=1500ns --wave=pc_tb.ghw")

# os.system("ghdl -a pc_tb.vhdl")
# os.system("ghdl -e pc_tb")

os.system("ghdl -a rom.vhdl")
os.system("ghdl -e rom")

os.system("ghdl -a rom_tb.vhdl")
os.system("ghdl -e rom_tb")

# os.system("ghdl -r rom_tb --stop-time=1000ns --wave=rom_tb.ghw")

os.system("ghdl -a rom_pc.vhdl")
os.system("ghdl -e rom_pc")

os.system("ghdl -a rom_pc_tb.vhdl")
os.system("ghdl -e rom_pc_tb")

# os.system("ghdl -r rom_pc_tb --stop-time=2000ns --wave=rom_pc_tb.ghw")

os.system("ghdl -a rom_pc_ula_register.vhdl")
os.system("ghdl -e rom_pc_ula_register")

os.system("ghdl -a rom_pc_ula_register_tb.vhdl")
os.system("ghdl -e rom_pc_ula_register_tb")

# os.system("ghdl -r rom_pc_ula_register_tb --stop-time=2000ns --wave=rom_pc_ula_register_tb.ghw")


os.system("ghdl -a control_unit.vhdl")
os.system("ghdl -e control_unit")


os.system("ghdl -a processador.vhdl")
os.system("ghdl -e processador")

os.system("ghdl -a processador_tb.vhdl")
os.system("ghdl -e processador_tb")

os.system("ghdl -r processador_tb --stop-time=3000ns --wave=processador_tb.ghw")
