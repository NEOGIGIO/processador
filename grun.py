import os
import sys

def run(duration=100000):
    os.system("rm -rf work-obj93.cf")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a reg7bits.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e reg7bits")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a reg8bits.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e reg8bits")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a reg1bit.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e reg1bit")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a register_bank.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e register_bank")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a ula.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e ula")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a ula_register.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e ula_register")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a pc.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e pc")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a rom.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e rom")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a rom_pc.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e rom_pc")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a rom_pc_ula_register.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e rom_pc_ula_register")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a state_machine.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e state_machine")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a ram.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e ram")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a control_unit.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e control_unit")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a processador.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e processador")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -a processador_tb.vhdl")
    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -e processador_tb")

    os.system("../ghdl-0.36-macosx-mcode/bin/ghdl -r processador_tb --stop-time={}ns --wave=processador_alt_tb.ghw --ieee-asserts=disable".format(duration))