library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom is
    port(
        clk : in std_logic;
        endereco : in unsigned(6 downto 0);
        dado : out unsigned(13 downto 0)
    );
end entity;

architecture a_rom of rom is
    type mem is array (0 to 127) of unsigned (13 downto 0);
    constant conteudo_rom : mem := (
    --=BEGIN

		-- ADDI R1 0
		0 => "00100010000000",
		-- ADDI R2 1
		1 => "00100100000001",
		others => (others=>'0')

    --=END
    );

  begin 
    process(clk)
    begin
        if(rising_edge(clk)) then
            dado <= conteudo_rom(to_integer(endereco));
        end if;
    end process;
end architecture;