library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- sel
-- 00 - adição
-- 01 - subtração
-- 10 - maior igual
-- 11 - multiplicação

entity ula is
    port(
        sel: in unsigned(1 downto 0);
        in0, in1: in unsigned(7 downto 0);
        saida: out unsigned(7 downto 0)
    );
end entity;

architecture a_ula of ula is
signal maior_igual: unsigned(7 downto 0);
signal multiplicacao: unsigned(15 downto 0);
begin
    multiplicacao <= in0 * in1;
    maior_igual <= "00000001" when in0 >= in1 and in0(7) = in1(7) else 
                   "00000001" when in0(7) < in1(7) else
                   "00000000";
    saida <=  in0 + in1 when sel = "00" else
            in1 - in0 when sel = "01" else
            maior_igual when sel = "10" else
            multiplicacao(7 downto 0) when sel = "11" else
            "00000000";
end architecture;